
import westrac from './images/westrac.png'
import coateshire from './images/coateshire.png'
import emeco from './images/emecologo.png'
import australia from './images/australia.jpg'

import wordcloud from './images/wordcloud.jpg'

import mineq from './images/mineq.png'
import mineqArchitecture from './images/mineqArchitecture'

import fms from './images/fms.jpg'

import coatesdata from './images/coatesdata.png'

import mineqlogo from './images/mineqlogo.png'
import quartileonelogo from './images/quartileonelogo.png'

import knuckleboom from './images/knuckleboom.jpg'
import telehandler from './images/telehandler.jpg'
import scissorlift from './images/scissorlift.jpg'

import coateshireutilization from './images/coateshireutilization.jpg'
import emecopremier from './images/emecopremier.png'
import premiertracking from './images/premier-tracking.png'
import premierArchitecture from './images/premierArchitecture.png'

import techmaturity from './images/technologymaturity.png'
import hypecycle from './images/hypecycle.png'

import coateshireArchitecture from './images/CoateshireArchitecture.png'
import coateshireDataReplicate from './images/CoatesHireDataReplication.png'

import dataArrival from './images/dataArrival.gif'

import trax from './images/trax.jpg'
import dart from './images/dart.png'

import utilizationSignal from './images/UtilizationSignal.png'

import scrumVsKandan from './images/scrumVsKanban.jpg'

import mineqHealth from './images/mineqEvents.png'

import conwayslaw from './images/conwayslaw.png'

import kafkaconsumption from './images/kafka-consumption.png'

import infrastructureseparated from './images/infrastructure-separated.png'

export const images = {
    westrac,
    coateshire,
    wordcloud,
    emeco,
    premiertracking,
    australia,
    mineq,
    fms,
    coatesdata,
    mineqlogo,
    quartileonelogo,
    knuckleboom,
    scissorlift,
    telehandler,
    coateshireutilization,
    techmaturity,
    emecopremier,
    coateshireArchitecture,
    coateshireDataReplicate,
    dataArrival, 
    trax,
    dart,
    hypecycle,
    mineqArchitecture,
    premierArchitecture,
    utilizationSignal,
    scrumVsKandan,
    mineqHealth,
    conwayslaw,
    kafkaconsumption,
    infrastructureseparated
}