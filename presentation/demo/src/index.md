import { images } from './images'
import { Image, Grid, Divider } from 'theme-ui'
import {Steps,Split,SplitRight,Invert} from 'deck/src/gatsbyplugin/components.js'
import {BouncyDiv} from './animation.js'
import { HeatmapView} from '../../src/heatmap-view.jsx'
import { BellCurveView } from '../../src/bellcurve-view.jsx'
import { ProcessCurveView } from '../../src/processcurve-view.jsx'

# Evolution of a Data Pipeline


<Split>


1. Talk Aim
2. Clients
3. Team
4. The Problem
5. Opportunity Viewpoint
6. Client Value Viewpoint
7. Initial Goal Viewpoint
8. Data Capture Viewpoint
9. Architectures Viewpoint
10. Project Status Viewpoint
11. Trends
12. Significant Problems
13. Data Replications
14. Handling Out of Order Data
15. Way of working
16. Demo Viewpoint
17. Project Outcomes
18. Take Aways


<Image src={images.wordcloud} width='900px'/>

</Split>

---
# Talk Aim

<Steps>

- My Aim: 
  - Presentation and Comparison of 3 separate remote data capture and visualization projects implemented at QuartileOne between 2018-2020
  - Provide some feel to the variances and commonality between the projects
  - Discussion of significant problems and solution approaches
  - Provide evidence for some take aways provided at the end. 

- Your Goal: Identify the project(s) which were a commercial success for both Quartile One and Customer
  - profitable , timely and providing customer value
  - before the reveal at the end

- Let me know of opportunities for these type of projects

</Steps>

---
# Clients

## Westrac
Caterpillar Dealer in Western Australia. Part of the Seven Group. *Wanting to figure out how IoT revolution could work for them.* 
<Image src={images.westrac} width='100px' />

## Coates Hire 
Large equipment rental company. Part of the Seven Group. *Wanting to figure out how IoT revolution could work for them.*
<Image src={images.coateshire} width='100px'/>

## Emeco 
Large equipment rental company. Publically listed company.  *Wanting to make better use of their IT Assets.* 
<Image src={images.emeco} width='100px'/>

---
# Team
## Integrated Solutions

- Dion Lucke - Developing and maintaining a team structure which allowed project learning and intellectual property evolution
- Mic Holt , Cameron Caesar, Jan du Pleiss - Working to translate non information technology problems into information technology solutions.
- Michael Mullen, Liam Maruff , Joel Smith, Brad Seeley - Rapid development of solutions which appealed to customers and evolved through time.
- Jason Mulvenna, Srimal Jayawardena - Consider approach to rapidly evolve data science visualization techniques.
- Phil Tomlinson - Operating and improving solutions delivered for multiple customers

---
# The Problem

<Split>

- Mining and Construction companies have operations that span large areas, sites, and even scale to the whole country.

<Image src={images.australia} width='500px' />

</Split>


***A significant engineering problem is to gather information in real time from remote equipment and deliver it to a cloud platform for subsequent analysis***

- Types of challenges include
  - proxying data through secure corporate networks
  - managing translation between different messaging systems
  - integration with Fleet Management systems or telemetry brokers hubs
  - integration with large / historian data platforms
  - maintaining 24x7 availability of software services that continuously receive data
  - supporting continuous solution change of when there is a continuous stream of data.
  - managing data arriving out of order when calculating statistics (e.g. utilization) based on ordered information
  - making data streams available for multiple purposes

---
# Opportunity Viewpoint

Develop solutions which captures utilization, location and asset health data from equipment into big data cloud storage.

```...sounds like a fleet management system...which already exist```

<Invert>
<Split>

While Fleet Management Systems exist:

- they are an expensive additonal cost generally only available for specific equipment from the OEM themselves
- they aren't available for smaller pieces of equipment
- they dont summarize/present information in ways appropriate to all teams

</Split>
</Invert>

And there are recent breakthroughs which mean there is opportunities to enter this market.

<Split>
<Image src={images.hypecycle}  width='450px'/>


<Image src={images.techmaturity} width='350px'/>

</Split>

- [Gartner Hype Cycle](https://www.mediabuzz.com.sg/research-jan-2015/gartner-hype-in-2015-around-the-internet-of-things-iot-and-wearables)
- [Technology Maturity]()

----
# Client Value Viewpoint

<Split>

<h2>Where are my assets and are my assets in use?</h2>

<Image src={images.coateshireutilization} width='900px' />

</Split>
<SplitRight>

<h2>Are my assets producing?</h2>

<Image src={images.emecopremier} width='900px' />

</SplitRight>
<Split>

<h2>Are my assets healthy?</h2>

<Image src={images.mineqHealth} width='900px' />

</Split>

---
# Initiative Goal Viewpoint

<Split>

## MineQ 

Capture engine diagnostic data from large mining equipment
<Image src={images.mineq} width='800px' />

</Split>
<SplitRight>

## CoatesHire 

Capture utilization and location from mobile rental equipment
<Image src={images.scissorlift} width='200px' padding="10px 10px 10px 10px" />
<Image src={images.telehandler} width='200px' padding="10px 10px 10px 10px" />
<Image src={images.knuckleboom} width='200px' padding="10px 10px 10px 10px" />
</SplitRight>
<Split>

## Emeco 
Capture shift utilization and production from coal production equipment

<Image src={images.premiertracking} width='900px'/>
</Split>

---
# Data Capture
## CoatesHire
<Invert>

<Split>

- Low Cost Digital Telemetry Device 
- GPS
- SIM Card
- Analog Input channel

<Image src={images.dart} padding='5px 5px 5px 5px' width='250px'/>
</Split>
</Invert>

## MineQ

<SplitRight>

- Ruggedized Linux PC
- Ubuntu OS
- Booted from USB
- Running Docker containers for multiple services
- Hooked into onboard data bus (VIMS , VIMS 3G D11T, J1939, GA Drive, SKF15 , BSC2, BSC3, GA Drive )

<Image src={images.trax} padding='5px 5px 5px 5px' width='500px'/>
</SplitRight>

## Premier

<Invert>
<Split>

Reliance upon FMS

</Split>
</Invert>

---
# Architecture

## Coateshire
<Image src={images.coateshireArchitecture} width='800px'/>

## MineQ
<Image src={images.mineqArchitecture} width='800px'/>

## Premier
<Image src={images.premierArchitecture} width='800px'/>

## Notice the repetition

### Ingest
- Store and Forward Problem
- Process and Transform Stream

### Processing
- Store and Forward Problem
- Process and Transform Stream
- Reprocess Data

### Application
- Store and Forward Problem
- Process and Transform Stream
- Reprocess Data

### Portal
- Process and Transform Stream
- Authentication Integration

---
# Project Stats


| Project | SubSystems | MicroServices | Storage | Integrations | Queue Mechanisms | APIs | Languages | Team Location | Data Points |
| ------- | ---------- | ------------- | ------- | ------------ | ---------------- | -----| --------- | ------------ | -----
| Premier | 3  (ingest, mapping, portal  ) | 5 | 2 (SQL, GraphQL) | 1 | 1 (Rabbit MQ) | 20 | 7 | 1 | 10 million |
| Coateshire | 3  (ingest, processing, portal ) | 7 | 2 (SQL, MongoDB) | 3 | 1 (EventHub) | 20 | 11 | 3  (Brisbane, Perth, UKraine) | 100 million |
| MineQ | 6  (device,gateway,ingest, processing, portal, configuration mgt ) | 15 | 4 (SQL, MongoDB, Pi, RTI) | 9 | 3 (RTI DDS, MQTT, IOT Hub) | 100 | 22 | 6 (Brisbane, Sydney, Perth, India, Ukraine)| 1000 million |

<Image src={images.coatesdata} width='600px' />

---
# Trends

<HeatmapView name='heatmap'/>

----
# Significant Problems

- ***How to make high quality data available during development?***
   - So that test activities are representative
   - Performance of the system is testable
   - Production issues can be reproduced

- ***How to handle data arriving out of order, sporadically, not at all***
    - So that information is timely
    - So that stats are not incorrect
        - Utilization (a running total) in particular

- ***How to effectively link the development process to the business value and the complexity to be delivered***
    - As the exact nature of what is required is not known until the system development is attempted
    - As the complexity of the system requires larger pool of resources spread across larger geography and time

---
# Replication of Data from Production into other environments 

<Invert>
Problem: How to make high quality data available during development?
</Invert>

Solution is to replicate live production stream in real time into other environments.

- you need multiple readers who want to be able to read independently.

This turns out to be easy using *Kafka* based queues and provides lots of flexibility.
  - Are able to take offline any 'environment' and not impact other environments
  - You can bring environments offline independently and have them catch up as they come back
  - You can take offline the data processing while data is being continuously received
  - You can recover from data processing bugs by reprocessing data streams
  - You can have different versions of the portal running in parallel

<Image src={images.coateshireDataReplicate} width='800px' />

---
# Handling Data Arrival Complexity 
## Problem: How to support data arrival complexity when computing aggregate statistic?
<Invert>

- Data out of order
- Different time lag
- Bursts of data
- Data arriving before meta data

</Invert>

<Grid width={[500,100,500]}>

<Image src={images.dataArrival} width='600px'/>

<Invert>

Solution is to use *Checkpoints* on each statistic unit (asset hire)
- periodic processes to update utilizations from the checkpoint
- checkpoints always lag most recent data
- continuously calculating statistics and updating checkpoints
- when problems occur need to back date checkpoints 

</Invert>

<Image src={images.utilizationSignal} width='600px'/>

</Grid>

---
# Way of Working / Structuring the development process

<Invert>
Problem: How to effectively link SDLC resources to delivery of value?
</Invert>

## Waterfall vs Scrum vs Kanban
<Image src={images.scrumVsKandan} width='800px' />

<Divider/>

<BellCurveView/>

- More combinations - shift right

<Divider/>

<ProcessCurveView/>


[Understanding Cycle Time Distribution Shapes](https://observablehq.com/@troymagennis/understanding-cycle-time-distribution-shapes)

[The Economic Impact of Software Development Process Choice -
Cycle-time Analysis and Monte Carlo Simulation Results](https://github.com/FocusedObjective/FocusedObjective.Resources/raw/master/Presentations/The-Economic-Impact-of-Software-Development-Process-Choice-Cycle-time-Analysis-and-Monte-Carlo-Simulation-Results.pdf)

---
# Demo Viewpoint

Video

---
# How did the 3 projects turn out

<Steps>

## MineQ
MineQ was effectively a break even for Quartile One but as such was a lost opportunity. It was shut down by the customer after 18 months in large part because the effort was larger than expected.

## Premier
Quartile One lost a considerable amount developing Premier but was delivered to the customer and is being used to this day.  
There are opportunities to deliver similar solutions.

## Coates Hire
The Coates Hire solution was both profitable for Quartile One and provided value to the customer.
The solution was operated for 12 months while Coates Hire developed their own version of the solution.
It has recently been shut down as the replacement came online.

</Steps>

---
# Takeways

For data capture, acquisition and presentation projects:

- Break work down *across projects* so that similar problems are solved once rather than repeatedly ... ***avoid Conways law***
  - For any project, before assigning resources allow capability teams to review the functionality ... so that repetition can be observed
    - A project plan should be described as extensions of capability with some integration between capabilities.

<Image src={images.conwayslaw} width='600px'/>    

- ***Minimize technology*** stack variety where possible / keep it simple .... so that resources can switch contexts easily
  - Consider a techology stack which provides a unifying paradigm across 
      - user experience
      - application/server side 
      - data analytics

- Maximize ease of testing end to end flow so that value is minimally coupled to infrastructure 
   - Try out ***[Red Green Refactor](https://www.codecademy.com/articles/tdd-red-green-refactor)***
      - With the *end to end* perspective
        - what tests express the solution
        - make the tests pass
        - improve the implementation

- Separate infrastructure from functionality ... ***so that end to end functionality can be expressed with minimal cognitive load***

<Image src={images.infrastructureseparated} width='600px'/>    

- ***Introduce Kafka*** based queueing mechanism ... for the flexibility it will provide

<Image src={images.kafkaconsumption} width='600px'/>

---

<Header>
<h2>Aurecon - Quartile One</h2>
</Header>

<Footer>
<h2>Evolution of a Data Pipeline</h2>
</Footer>

Thank you.