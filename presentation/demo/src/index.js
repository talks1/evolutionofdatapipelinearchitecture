import React from 'react';
import { render} from 'react-dom';

import * as themes from 'deck/src/themes/index.js'
import { components }  from 'deck/src/gatsbyplugin/index.js'

import pressotheme from './presso-theme'

import Content from './index.md'
const { SlideDeck } = components


const App = (props) => {
    return <SlideDeck
        location={location}
        theme={pressotheme}
        navigate={()=>{}}
        animationMode='rotate'
        children={Content({components}).props.children}
    >
    </SlideDeck>
}

render(<App />, document.querySelector('#demo'));

