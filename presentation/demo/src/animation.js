import React from 'react'
import styled, { keyframes } from 'styled-components';
import { bounce, fadeIn, flipInX, rotateOutUpRight, slideInRight } from 'react-animations';
 
const bounceAnimation = keyframes`${bounce}`;
const bouncyDiv = ()=>styled.div`animation: 1s ${bounceAnimation};`
export const BouncyDiv = styled.div`animation: 1s ${bounceAnimation};`

const fadeInAnimation = keyframes`${fadeIn}`; 
const fadeInDiv = ()=>styled.div`animation: 5s ${fadeInAnimation};`

const flipInAnimation = keyframes`${flipInX}`;
const flipInDiv = ()=>styled.div`animation: 5s ${flipInAnimation};`

const rotateAnimation = keyframes`${rotateOutUpRight}`;
const rotateDiv = ()=>styled.div`animation: 5s ${rotateAnimation};`

const slideAnimation = keyframes`${slideInRight}`;
const slideDiv = ()=>styled.div`animation: 1s ${slideAnimation};`

