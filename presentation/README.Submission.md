# Submission - Evolution of Data Pipeline Architecture

# About

This is content proposed for submission to  Exemplar Forum 2020 - Call for submissions
Raising the game with machine learning and data analytics
 [Submission Form](https://forms.office.com/Pages/ResponsePage.aspx?id=sQe9wba3ykee8M9mc12q5ptnOr_U0JtClFQSn8IwM-tURUY1T1E3MjFBUUREQko0VFMzQ1RIV0ZSQSQlQCN0PWcu)

## Category

Data Pipeline Development

## Client 

Westrac, CoatesHire, Emeco, Internal

## Business Problem

Mining and Construction companies have operations that span large areas, sites, and even scale to the whole country.
These present problems to centrally monitor and manage the utilization of equipment fleets.
A significant technical problem is to gather information in real time from remote equipment and deliver it to a cloud platform for subsequent analysis.
Types of challenges include
- proxying data through secure corporate networks
- managing translation between different messaging systems
- integration with Fleet Management systems or telemetry brokers hubs
- integration with large / historian data platforms 
- maintaining 24x7 availability of software services that continuously receive data
- supporting continuous agile solution change of when there is a continuous stream of data.
- managing data arriving out of order when calculating statistics (e.g. utilization) based on ordered information
- making data streams available for multiple purposes

## Project Goals

Quartile One has been engaged in several projects aimed at delivering varying solutions for remote data acquisition into cloud visualization solutions 

## Business Impact

Quartile One has evolved its solution architecture through its successes and failures in deliverying several remote data acquisution projects.
In particular
- shift away from using traditional enterprise messaging systems (RabbitMQ) to distributed log messaging systems (Kafka and Kafka based cloud services)
- shift from away large SQL/NoSQL database storage to log storage as the primary storage mechanism 
- shift away from manually configured compute architecture to source control defined deployment architecture (Kuberneters)
- retain microservice deployment but allow for end to end development and testing as a monolith
- move away from 'smart' back end query to loading large datasets into the front end
- move away from prescriptive design led thinking into a lean agile evolutionary methodology 

## Key People

- Dion Lucke - Developing and maintaining a team structure which allowed project learning and intellectual property evolution
- Mic Holt , Cameron Caesar, Jan du Pleiss - Working to translate non information technology problems into information technology solutions.
- Michael Mullen, Liam , Joel Smith, Brad Seeley - Rapid development of solutions which appealed to customers and evolved through time.
- Jason Mulvenna, Srimal - Consider approach to rapidly evolve data science visualization techniques.
- Phil Tomlinson - Operating and improving solutions delivered for multiple customers

## Project Start - End

Quartile one has been engaged in these projects through 2018 and 2019.

## Metholodogy 

The Quartile Team has migrated towards solution delivery as a service using an agile kanban approach.
We try to minimize planning but see value in repetitive consideration of expected upcoming work and prioritization of that work.
We also strive towards test driven development but with an emphasis on testing end to end transactions which provide value rather than tests which lock in technology choices which are not related to the value being delivered. 

## Technology 

- Messaging 
  - Distributed - RTI DDS
  - Enterprise - RabbitMQ 
  - Distributed Log Messaging = Kafka/EventHub
  - Peer to Peer Distribute Log Messaging - DAT HyperCore/Swarm
- Storage and Query - 
  - Relational - Azure SQL Server, Google Postgres
  - NoSQL - Mongo DB
- Compute
  - Azure App Service
  - Azure Kubernetes
  - Google Kubernetes
- API
  - REST APIs
  - GraphQL
- Code Platform
  - Nodejs, .Net
- UI Framework
  - Angular, Vue.js, Ember, React
- Visualization
  - D3

# Technical Results

Each project (MineQ,CoatestHire and EOS Premier) produced systems to aggregate equipment data into the cloud along with a web application to present the information.

The MineQ project was most ambitious project. 
It developed software for embedded ruggedized linux PC mounted to heavy mining equipment and connected to can bus system. 
It developed gateway software to interface with the embedded PC and forward data through to Azure cloud services.  
Finally it deployed large scale Mongo DB clusters and web applications and apis to allow access to the collected data.
[MineQ Overview Diagram](https://quartileone.atlassian.net/wiki/download/thumbnails/577897/Arch_1.png?version=1&modificationDate=1489459328850&cacheVersion=1&api=v2&width=1200&height=1140)

The CoatesHire solution integrated with on equipment telemetry device via a cloud telemetry data broker.  
It collected location and utilization data into a cloud MongoDB solution via EventHub log queues.
Back end services aggregated the data into a SQL Server database serving data specific to a web application used by users. 
[Coates Hire You Tube Video](https://quartileone.atlassian.net/wiki/download/attachments/573161/2019-07-01-coateshire-programme-overview.pptx?version=3&modificationDate=1562551296796&cacheVersion=1&api=v2)

The EOS Premier solution integrated with a Fleet Management system and retrieved production and utilization information.
This information was processed and aggregated into a postgress database. 
The information was queried by a web application using a GraphQL back end.
[EOS Premier UI Screenshots](blob:https://quartileone.atlassian.net/968f1d64-1ca7-4e19-aee2-750eaddca599#media-blob-url=true&id=549f8493-8adf-4509-8ce2-4badacf6ff3f&collection=contentId-3605385&contextId=3605385&mimeType=image%2Fpng&name=image-20190712-051130.png&size=1114942&width=4446&height=2438)

# Deployment

The MineQ project deliverables were used to progress the solution concept for several customers.

The CoatesHire solution has been used by the customer for the entire 2019 and is still live as of February 2020. CoatesHire are now re-developing the solution following learnings taken from the current solution.

The EOS Premier solution continues to be used on a shift by shift basis by staff at the Premier Coal mine.

## Next Steps

- Update the [Integrated Operations Technology Stack](https://gitlab.com/q1-team/guidelines/-/blob/master/Technology%20Stack.md) to reflect learnings.
- Use the proposed architecture for internal green field projects when there are technology choices available.
- Describe proposed architecture in a customer presentable form as customer proposals are prepared.

