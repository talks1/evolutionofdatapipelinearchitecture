import { describe } from 'riteway';
import { Projects } from '../src/model/Projects'

describe('Projects Model', async (assert) => {

  let projects = Projects('Root')
  let p = projects.Project('P1')
    .Profitability(1)
    .CustomerValue(1)
    .Timeliness(18)
    .Resource(10/*Team*/)
    .Languages('Javascript,Angular,C#,Html,Css, ARM Templates, RTI, C++, Linux, Azure, Windows, Docker, D3, SQL, Mongo, IOTHub,OpenId, GoogleMaps, Pi, Bash, DeviceController, VIMS, J1939,')
    .Storage('Mongo, SQL, Pi, RTI')
    .Schema(100)
    .QueueMechanism('RTI DDS,AMP, IOT Hub')
    .TeamLocations("Brisbane, Sydney, Perth, Ukraine, India")
    .MicroService('TraxDeviceManager,TraxVIMS,TraxJ1939,TraxDDS, FieldBusGateway, EventSerice,EvenReceiver, Security, Audit, ConfigurationService, Portal')
    .SDLC('Waterfall')
  .Done()

  console.log(projects.Children())
    
  assert({
    given: 'projects',
    should: 'be projects',
    actual: true,
    expected: true
  });
});
