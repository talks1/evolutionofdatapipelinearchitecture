import { describe } from 'riteway';
import { interpolate, Model2 } from '../src/heatmap-component'


describe.only('Heatmap-Model', async (assert) => {

  const model = Model2([[0,0],[0.5,0.5],[1,1]])
  
  let z = model.z(0,0)

  console.log(z)

  assert({
    given: 'projects',
    should: 'be projects',
    actual: true,
    expected: true
  });
});


describe('Heatmap', async (assert) => {

  const model = Model()
  let data = interpolate({x1:0, y1:0, x2:10,y2:10},model)
  
  console.log(data)
  assert({
    given: 'projects',
    should: 'be projects',
    actual: true,
    expected: true
  });
});
