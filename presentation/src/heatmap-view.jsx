import React, { useReducer , useState, useEffect } from 'react'
import { HeatmapComponent } from './heatmap-component'

import './styles.css'

import { projects } from './data/projects'
import Select from 'react-select';
import { extent } from 'd3-array'
import { scaleLinear } from 'd3-scale'

const yOptions = ['profitability','customerValue','timeliness'].map(axis=>({value:axis,label:axis}))
const xOptions = ['schema','resource','languages','queueMechanism','teamLocations','microService','agileness','datapoints','subsystems'].map(axis=>({value:axis,label:axis}))
  
const rOptions = ['linear','polynomial','none'].map(axis=>({value:axis,label:axis}))

export const makeSampleData = (rawData,dSelections)=>{
  if(dSelections.length===1){
    return rawData.map(p=>p.axis[dSelections[0].value])
  }else {
    let numSamples = rawData.length
    let scaledUnits = dSelections.map(dSelection=>{
      let dOriginal = rawData.map(p=>p.axis[dSelection.value])
      const dExtents = extent(dOriginal, d=>d)
      const dScaleUnit = scaleLinear()
        .domain(dExtents)
        .range([0,1])
      return dOriginal.map(d=>dScaleUnit(d))
    })
    let initial = Array(numSamples).fill(0);
    let summed = scaledUnits.reduce((h,n)=>{
      return n.map((item,index)=>h[index]+item)
      },initial)//sum
    let avg = summed.map(s=>s/dSelections.length)
    return avg
  }
}

export const HeatmapView = ({name})=>{

  const [ xSelections, setXSelections] = useState([xOptions[0]])
  const [ ySelections, setYSelections] = useState([yOptions[0]])
  const [ rSelection, setRSelection] = useState(rOptions[0])

  let sampleDataX = makeSampleData(projects.children,xSelections)
  let sampleDataY = makeSampleData(projects.children,ySelections)
  let sampleDataXY = sampleDataX.map((x,index)=>[x,sampleDataY[index]])

  const [sampleData,setSampleData] = useState(sampleDataXY)

  useEffect(()=>{
    // console.log(xSelection.value)
    // console.log(ySelection.value)    
    let sampleDataX = makeSampleData(projects.children,xSelections)
    let sampleDataY = makeSampleData(projects.children,ySelections)
    sampleDataXY = sampleDataX.map((x,index)=>[x,sampleDataY[index]])
    setSampleData(sampleDataXY)
  },[xSelections,ySelections,setSampleData])

  function xChange(option){
    setXSelections([option])
  }
  function yChange(option){
    setYSelections(option)
  }
  function rChange(option){
    setRSelection(option)
  }

  return <div>
      Regression Type : <Select
        value={rSelection}
        onChange={rChange}
        options={rOptions}
      />
      Y Axis: <Select
        value={ySelections}
        onChange={yChange}
        options={yOptions}
        isMulti
      />
      <HeatmapComponent name={name} regressionType={rSelection.value} sampleData={sampleData}/>
      X Axis: <Select
        value={xSelections}
        onChange={xChange}
        options={xOptions}
      />
      </div>
}
