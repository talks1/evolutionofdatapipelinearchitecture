import React, { useEffect, useState } from 'react';

import { format } from 'd3-format'
import {select} from 'd3-selection'
import { hierarchy, partition } from 'd3-hierarchy'
import {scaleOrdinal, scaleLinear,scaleQuantize} from 'd3-scale'
import {schemeCategory10} from 'd3-scale-chromatic'
import {transition} from 'd3-transition'
import { axisBottom, axisLeft } from 'd3-axis'
import { extent, max,min, histogram } from 'd3-array'

export const HistogramComponent =  (options) => {

	const { name, sampleData, width=400, height=200} = options
	const margin = {top: 20, right: 20, bottom: 30, left: 40}

	const [theVis, setVis] = useState(null);
	let thevis = theVis

	const setup = () => {

		if(!thevis){
			thevis = visualization(`#${name}`, {
				//size: 300,
			});
			thevis.render(sampleData);
			setVis(thevis)
		}
		thevis.update(sampleData)
	}

	var visualization = function(container, configuration) {
		var that = {};
		var config = {
			width,
			height
		};

		function configure(configuration) {
			var prop = undefined;
			for ( prop in configuration ) {
				config[prop] = configuration[prop];
			}
			
		}
		that.configure = configure;

		// const extents = {	x1: margin.left, y1: margin.top, x2: config.width - margin.right, y2: config.height - margin.bottom} 
		// const cell = Math.min(extents.x2-extents.x1,extents.y2-extents.y1)/coarseness
	  
		function render(sData) {
			that.svg = select(container)
				.append('svg:svg')
					.attr('class', 'visualization')
					.attr('width', config.width)
					.attr('height', config.height)
					.attr('viewBox', [0, 0, config.width, config.height])
					.style('font', '16px sans-serif');
						
			update(sData);
		}
		that.render = render;
	
		function update(sData) {
		
			const xScale = scaleLinear()
				.domain(extent(sData)).nice()
				.range([margin.left, config.width - margin.right]);  

			const xTicks = xScale.ticks(50)
			const bins = histogram()
				.domain(xScale.domain())
				.thresholds(xTicks)
				(sData);

			const yScale = scaleLinear()
				.domain([0, max(bins, d => d.length)]).nice()
				.range([config.height - margin.bottom, margin.top]);
			//yScale.ticks(3)
			

			// svg.append("g")
			// 	.attr("transform", `translate(0,${height - margin.bottom})`)
			// 	.call(d3.axisBottom(x).tickSizeOuter(0));
		  
			
			// svg.append("g")
			// 	.attr("transform", `translate(${margin.left - 6},0)`)
			// 	.call(d3.axisLeft(y).ticks(1))
			// 	.call(g => g.select(".domain").remove());
		  

			const xAxis = g => g
				.attr("transform", `translate(0,${config.height - margin.bottom})`)
				.call(axisBottom(xScale).ticks(config.width / 80, "f"))
				.call(g => g.select(".domain").remove())
				.call(g => g.append("text")
					.attr("x", config.width - margin.right)
					.attr("y", -4)
					.attr("fill", "currentColor")
					.attr("font-weight", "bold")
					.attr("text-anchor", "end")
					//.text(data => interpolatedData.xaxis)
				)

			const yAxis = g => g
				.attr("transform", `translate(${margin.left},0)`)
				.call(axisLeft(yScale).ticks(null, "f"))
				.call(g => g.select(".domain").remove())
				.call(g => g.append("text")
					.attr("x", 4)
					.attr("y", margin.top)
					.attr("dy", ".71em")
					.attr("fill", "currentColor")
					.attr("font-weight", "bold")
					.attr("text-anchor", "start")
					//.text(data=>interpolatedData.yaxis)
					)

			var colorScale = scaleQuantize()
					.domain([0,1])
					.range(["#FFFFFF", "#AAAAAA","#888888","#444444","#000000"]);
			
					
				that.svg.selectAll('*').remove()

				const bar = that.svg.append("g")
				.attr("fill", "steelblue")
				.selectAll("rect")
				.data(bins)
				.join("rect")
					.attr("x", d => xScale(d.x0) + 1)
					.attr("width", d => Math.max(0, xScale(d.x1) - xScale(d.x0) - 1))
					.attr("y", d => yScale(d.length))
					.attr("height", d => yScale(0) - yScale(d.length));

				that.svg.append("g").call(xAxis);
				that.svg.append("g").call(yAxis);
		}
		that.update = update;

		configure(configuration);
			
		return that;
	};

  useEffect(setup)
  
  return <div id={name}></div>
}




