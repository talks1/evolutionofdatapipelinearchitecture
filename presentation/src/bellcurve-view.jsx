import React, { useReducer , useState, useEffect } from 'react'
import { HistogramComponent } from './histogram-component'

import Select from 'react-select';
// import { extent } from 'd3-array'
// import { scaleLinear } from 'd3-scale'

const diceOptions = ['1','2','4','8','16'].map(axis=>({value:axis,label:axis}))

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function diceAdd(n) {
  var v = 0
  var i
  for (i = 0; i < n; i++) 
    v = v + getRandomInt(1,7);
  return v
}

const makeSampleData = (option)=>{
  return Int8Array.from({length: 10000}, x => diceAdd(option.value))
}

export const BellCurveView = ({name='bellcurve'})=>{

  const [ diceSelections, setDiceSelections] = useState(diceOptions[0])
  
  let initialData = makeSampleData(diceSelections)
  const [sampleData,setSampleData] = useState(initialData)

  useEffect(()=>{
    let sData = makeSampleData(diceSelections)
    setSampleData(sData)
  },[diceSelections,setSampleData])

  function diceChange(option){
    setDiceSelections(option)
  }
  
  return <div>
      How many standard 6 side dice to add: <Select
        value={diceSelections}
        onChange={diceChange}
        options={diceOptions}
      />
      <HistogramComponent name={name} sampleData={sampleData} width="400" height="200" />
      </div>
}
