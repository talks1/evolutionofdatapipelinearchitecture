import React, { useEffect, useState } from 'react';

export const CSVDownloadComponent =  ({ link,data,labels,props }) => {

	// let labels = [ 'parent','id','name','type','comment','question','permissions','status' ]
	// let props = [ x=>{ return x.parent? x.parent.id : ''},'id','name','type','comment','question','permissions' ,'status' ]

	let datarows = data.map(x=>{
		let result = props.map(p=>{
			let t = typeof p

			if(t === 'function'){
				return p(x)
			}
			else {
				return x[p]
			}
		})
		return result
	})

	let rows = [ labels ].concat(datarows)

	//console.log(rows)

	let csvContent = 'data:text/csv;charset=utf-8,' + rows.map(e => e.join(',')).join('\n');

	let download = 'raw.csv'

	var encodedUri = encodeURI(csvContent);
	return <a href={encodedUri} download={download}>{link}</a>
}

