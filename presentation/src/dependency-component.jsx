import React, { useEffect, useState } from 'react';

import {select,event} from 'd3-selection'
import {scaleOrdinal, quantize} from 'd3-scale'
import {schemeCategory10} from 'd3-scale-chromatic'
import {forceSimulation,forceLink,forceManyBody,forceX,forceY} from 'd3-force'
import {drag} from 'd3-drag'

import { Filter } from './control/Filter'
import { array } from './model/Task'

const color = scaleOrdinal(schemeCategory10)

export const DependencyComponent =  ({ name,nodes,links }) => {

  const [theVis, setVis] = useState(null);
  let thevis = theVis
  let setup = () => {
	if(!thevis){
		thevis = visualization(`#${name}`, {
			//size: 300,
		});
		thevis.render();
		setVis(thevis)
	}
	thevis.update(nodes)
  }

  let taskTable = nodes

  let xfilter = Filter().Dimension('type',x=>x.type)
		//.Dimension('resourceGroup',x=>x.resourceGroup)
		//.Dimension('isReference',x=>x.isReference)
		.Data(taskTable)
  
  let types = xfilter.DimensionGroup('type').all().map(x=>x.key).filter(x=>x.length>0)

  var visualization = function(container, configuration) {
	var that = {};
	var config = {
		width: 1000,
		height: 600
	};

	function configure(configuration) {
		var prop = undefined;
		for ( prop in configuration ) {
			config[prop] = configuration[prop];
		}
		
	}
	that.configure = configure;

	function render(newValue) {
		that.svg = select(container)
			.append('svg:svg')
				.attr('class', 'visualization')
				.attr('width', config.width)
				.attr('height', config.height)
				.attr('viewBox', [0, 0, config.width, config.height])
				.style('font', '16px sans-serif');
		
		const simulation = forceSimulation(nodes)
		.force('link', forceLink(links).id(d => d.id).distance(100))
		.force('charge', forceManyBody().strength(-500))
		.force('x', forceX(config.width/2))
		.force('y', forceY(config.height/2))

		that.svg.append('defs').selectAll('marker')
			.data(types)
			.join('marker')
			.attr('id', d => `arrow-${d}`)
			.attr('viewBox', '0 -5 10 10')
			.attr('refX', 15)
			.attr('refY', -0.5)
			.attr('markerWidth', 6)
			.attr('markerHeight', 6)
			.attr('orient', 'auto')
			.append('path')
			.attr('fill', color)
			.attr('d', 'M0,-5L10,0L0,5');

		const link = that.svg.append('g')
			.attr('fill', 'none')
			.attr('stroke-width', 1.5)
			.selectAll('path')
			.data(links)
			.join('path')
			.attr('stroke', d => color(d.type))
			.attr('marker-end', d => {
				return `url(${new URL(`#arrow-${d.target.type}`, location)})`
			})

		const node = that.svg.append('g')
			.attr('fill', 'currentColor')
			.attr('stroke-linecap', 'round')
			.attr('stroke-linejoin', 'round')
			.selectAll('g')
			.data(nodes)
			.join('g')
			.attr('class',d=>d.type)
			.call(selectionDrag(simulation));

		node.append('circle')
			.attr('stroke', 'white')
			.attr('stroke-width', 1.5)
			
			.attr('fill',d=>color(d.type))
			.attr('r', 6);

		node.append('text')
			.attr('x', 8)
			.attr('y', '0.31em')
			.text(d => d.id)
			.clone(true).lower()
			.attr('fill', 'none')
			.attr('stroke', 'white')
			.attr('stroke-width', 3);

		simulation.on('tick', () => {
			link.attr('d', linkArc);
			node.attr('transform', d => `translate(${d.x},${d.y})`);
		});
			
		update(newValue === undefined ? 0 : newValue);
	}
	that.render = render;
	
	function update(newValue, newConfiguration) {
		if ( newConfiguration  !== undefined) {
			configure(newConfiguration);
		}
		if(newValue.type){
			let clazz = `.${newValue.type}`
			let t = that.svg.selectAll(clazz)
			t.selectAll('circle').transition().duration(750).attr('r',20).transition().duration(750).attr('r',6)
		}	
	}
	that.update = update;

	function linkArc(d) {
		const r = Math.hypot(d.target.x - d.source.x, d.target.y - d.source.y);
		return `
			M${d.source.x},${d.source.y}
			A${r},${r} 0 0,1 ${d.target.x},${d.target.y}
		`;
	}

	const selectionDrag = simulation => {
		function dragstarted(d) {
			if (!event.active) simulation.alphaTarget(0.5).restart();
			d.fx = d.x;
			d.fy = d.y;
		}
		
		function dragged(d) {
			d.fx = event.x;
			d.fy = event.y;
		}
		
		function dragended(d) {
			if (!event.active) simulation.alphaTarget(0);
			d.fx = null;
			d.fy = null;
		}
		
		return drag()
			.on('start', dragstarted)
			.on('drag', dragged)
			.on('end', dragended);
	}
	
	configure(configuration);
	
	return that;
  };
 

  function filterType(x){
	
	thevis.update({type:x.target.innerHTML})
}

  useEffect(setup)
  
  return <div>
		{ types.map(x=>{
			let style = {color: color(x)}
			return <button key={x} onClick={filterType} style={style}>{x}</button>
			})
		}
		<div id={name}></div>
  </div>
}




