import crossfilter from 'crossfilter2'

export function ZoneFilterFactory(entities){
  var filter = crossfilter(entities),
        allGroup = filter.groupAll(),
        typeDimension = filter.dimension(function(d) { return d.type ? d.type : ''; })
        // priorityGroup = priorityDimension.group(Math.floor)

  return {
      crossFilter: filter,
      allGroup,
      typeDimension
      //priorityGroup
  }
}

