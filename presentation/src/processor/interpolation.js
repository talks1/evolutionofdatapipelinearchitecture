

export const interpolate1 = ({x1,x2,y1,y2}) =>{
	let coarseness = 30
	let dx = x2-x1 
	let dy = y2-y1 
	let min = Math.min(dx,dy)
	let result = [...Array(coarseness * coarseness)]
	
	return {
		xaxis: 'axiz1',
		yaxis: 'axis',
		data: result.map((item,index)=>{
			let x = Math.floor(index/coarseness)
			let y = index - x*coarseness
			return {
				x: x/coarseness, y: y/coarseness, height: (x/coarseness)*(y/coarseness) * min/(2*coarseness)
			}
		})
	}
}

