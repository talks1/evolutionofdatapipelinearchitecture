
import { Tasks,Dimension } from '../model/Task'

import { tasks } from './tasks'

//export const securitycontrols = Tasks('SecurityControls').Realizations(tasks)

export const securitycontrols = Dimension('securedBy',tasks)
    .$('GeoDocsSPA')
        .$('AzureAD')
        .Done()
    .Done()
    
    .$('FacadeWebApp')
        .$('FacadeAppService').Done()
    .Done()
    .$('UsersController')
            .$('AzureAD').Done()
        .Done()
        .$('DocumentsController')
            .$('AzureAD').Done()
        .Done()
        .$('ForgeController')
            .$('AzureAD').Done()
        .Done()

    .$('AutoDeskForgeAPI')
        .$('AutoDeskForgeAccount')
            .$('AutoDeskForgeAccountCredentials').Done()
        .Done()
    .Done()
    .$('ArcGISAPI')
        .$('ArcGISAccount')
            .$('ArcGISAccountCredentials').Done()
        .Done()
    .Done()

    .$('SqlDBExchange')
        .$('SQLExchangeFirewall').Done()
    .Done()
    .$('ManawautaTeam')
        .$('AzureAD').Done()
    .Done()
    
