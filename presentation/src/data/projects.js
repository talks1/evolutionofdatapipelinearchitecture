
import { Projects, Scale } from '../model/Projects'

const POSITIVE=1
const NEUTRAL=0
const NEGATIVE=-1

const ProfitabilityScale = Scale().Unit('Unit')
const CustomerValueScale = Scale().Unit('Unit')
const TimelinesScale = Scale().Unit('Months')
const ResourceScale = Scale().Unit('Teams')

export const projects = Projects()

    projects
        .Project('MineQ')
            .Profitability(NEUTRAL)
            .CustomerValue(NEGATIVE)
            .Timeliness(18)
            .DataPoints(1000)
            .Resource(10/*Team*/)
            .Languages('Javascript,Angular,C#,Html,Css, ARM Templates, RTI, C++, Linux, Azure, Windows, Docker, D3, SQL, Mongo, IOTHub,OpenId, GoogleMaps, Pi, Bash, DeviceController, VIMS, J1939')
            .Storage('Mongo, SQL, Pi, RTI')
            .Schema(100)
            .QueueMechanism('RTI DDS,AMP, IOT Hub')
            .TeamLocations("Brisbane, Sydney, Perth, Ukraine, India")
            .MicroService('TraxDeviceManager,TraxVIMS,TraxJ1939,TraxDDS, FieldBusGateway, EventSerice,EvenReceiver, Security, Audit, ConfigurationService, Portal')
            .Agileness(0.25)//Waterall
            .SubSystems('Security, Device, Gateway, Ingest, Process, Storage, Portal, Release, Event')
        .Done()

        .Project('CoatesHire')
            .Profitability(POSITIVE)
            .CustomerValue(POSITIVE)
            .Timeliness(4)
            .DataPoints(100)
            .Resource(1/*Team*/)
            .Languages('Javascript,Vue, Html,Css, Azure, Kubernetes, Linux, Docker, SQL, Mongo, GoogleMaps')
            .Storage('Mongo, SQL')
            .QueueMechanism('Event Hub')
            .TeamLocations("Brisbane, Perth")
            .MicroService('Portal,HireService,REceived,Forwarder, Data, HireProcessor, Security')
            .Schema(10)
            .Agileness(1) //Kanban
            .SubSystems('Security, Ingest, Process, Storage, Portal')
        .Done()

        .Project('Premier')
            .Profitability(NEGATIVE)
            .CustomerValue(POSITIVE)
            .Timeliness(2)
            .DataPoints(10)

            .Resource(0.5/*Team*/)
            .Languages('Javascript,Ember, Html,Css, Windows, SQL, RabbitMq')
            .Storage('Sql, GraphQL')
            .QueueMechanism('Rabbit')
            .TeamLocations("Brisbane")
            .MicroService('Portal, GraphQL, Agent, Mapping, Modelling')
            .Schema(20)
            .Agileness(0.75)//Semi
            .SubSystems('Ingest, Process, Storage, Portal') 
        .Done()


