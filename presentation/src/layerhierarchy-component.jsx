import React, { useEffect, useState } from 'react';
import { line as d3line ,curveBundle } from 'd3-shape'
import {select,event} from 'd3-selection'
import {scaleLinear} from 'd3-scale'
import { drag as d3drag } from 'd3-drag'
import {zoom,zoomIdentity} from 'd3-zoom'
import {tree,stratify} from 'd3-hierarchy'
import { computeDependencyHierarchy } from './processor/compute-dependency-hierarchy.js'

export const LayerHierarchyComponent  =  ({ name,data, coordinator,width=1200,height=800,metadata=[],mapdata=[] }) => {
  
  var TRANSITION_DURATION = 500;
  var OPACITY = {
      NODE_DEFAULT: 0.7,
      NODE_FADED: 0.0,
      NODE_HIGHLIGHT: 1,
      LINK_DEFAULT: 0.5,
      LINK_FADED: 0.0,
      LINK_HIGHLIGHT: 0.8
    }

  var svg;
  var scaleX,scaleY;

  var line = d3line().x(function(d){return d.x;}).y(function(d){return d.y;});
  //var line = d3.linkVertical().x(function(d){return d.x;}).y(function(d){return d.y;});
  var cline = d3line().curve(curveBundle.beta(0.85)).x(function(d){
    return d.x;}).y(function(d){return d.y;})

 var tooltip

  var displayedNodeIndex={}
  var mapping,ilinks;
  var ilinkdata,ilinkindex

  var selectionFocus=false

  let hierarchyContexts = [];

  function makeHierarchy(list){
    var h = stratify()
      .id(function(d) { return d.id; })
      .parentId(function(d) {
        return d.parent=='null'? null : d.parent })(list);
      h.sum(function(d) {
        return d.size;
      })
      .sort(function(a,b){
        var order = Number(a.data.priority) > Number(b.data.priority) ? -1 : 1
        //console.log(a.data.name+":"+a.data.priority+"   "+b.data.name+":"+b.data.priority+"=>"+order);
        return order;
      })
      .eachAfter(function(node){
        //This rolls up all the descendants ids into the leafIds field
        node.data.leafIds=[];
        if(!node.children)
          node.data.leafIds =[ node.id ];
        else {
          node.children.forEach(function(child){
            node.data.leafIds = node.data.leafIds.concat(child.data.leafIds)
          })
        }
      })
      return h;
  }

  var makePopulationIntoHierarchies = function(layername,nodes,mapping){
    var filterednodes = nodes.filter((n)=>selectionFocus? n.visibility=='on' : true)
    var layers = computeDependencyHierarchy(layername,filterednodes,mapping)
    var roots=[]
    layers.forEach(function(lh){
      roots.push(makeHierarchy(lh))
    })
    return roots;
  }

  function navigate(node) {
    window.location = '#'+ node.id;
  }

  function fadeView(nodeSelection,linkSelection) {
      nodeSelection.attr('opacity', OPACITY.NODE_FADED)
      linkSelection.attr('opacity', OPACITY.LINK_FADED)
  }

  function defaultView(nodeSelection,linkSelection) {
      nodeSelection.attr('opacity', OPACITY.NODE_DEFAULT)
      linkSelection.attr('opacity', OPACITY.LINK_DEFAULT)
  }

  function highlightView(nodeSelection,linkSelection,n) {
    nodeSelection.attr('opacity', OPACITY.NODE_HIGHLIGHT)
    linkSelection.attr('opacity', OPACITY.LINK_HIGHLIGHT)
  }

  var showDependencies=true
  // coordinator.on("toggleDependencies",function(state){
  //   showDependencies = !showDependencies
  //   coordinator.call("modelchange")
  // })

  function hideTooltip() {
    tooltip.selectAll('*').remove();
    return tooltip.transition()
      .duration(TRANSITION_DURATION)
      .style('opacity', 0);
  }

  function showTooltip() {
    return tooltip
      .style('left', event.pageX + 20+  'px')
      .style('top', event.pageY - 20 + 'px')
      .transition()
      .duration(TRANSITION_DURATION)
      .style('opacity', 1);
    }

  let render= ()=>{
        var chart = select(`#${name}`)
        svg = chart.append('svg')
        //.attr("xmlns:xlink","http://www.w3.org/1999/xlink")
        .attr('width', width)
        .attr('height', height)

        tooltip = chart.append('div').attr('id', 'capabilitymaptooltip');

        svg.append('defs').append('path').attr('id','cpath').attr('d','M -20,0 A 10,10 0 0,1 20,0 A 10,10 0 0,1 -20,0')

        svg.append('marker').attr('id','triangle')
          .attr('refX',20)
          .attr('refY',6)
          .attr('markerWidth','13')
          .attr('markerHeight','13')
          .attr('orient','auto')
          .attr('markerUnit','strokeWidth')
          .append('path').attr('d','M2,2 L2,10 L8,6 L2,2').attr('style','fill: black')

        var transform = svg.append('g');

        mapping = mapdata
        ilinks = transform.append('g').attr('id','ilinks');

        scaleX = scaleLinear().domain([0,1]).range([0, width])
        scaleY = scaleLinear().domain([0,1]).range([0,height])

        var dragzoomtransform=zoomIdentity;

        function drag(){
          if(event){
            dragzoomtransform = dragzoomtransform.translate(event.dx,event.dy)
            transform.attr('transform', dragzoomtransform)
          }
        }
        function zoomed() {

          dragzoomtransform =  dragzoomtransform.scale(1/dragzoomtransform.k)
          dragzoomtransform =  dragzoomtransform.scale(event.transform.k)
          transform.attr('transform',dragzoomtransform)
        }
        svg.call(d3drag().on('drag',drag))
        svg.call(zoom().scaleExtent([0.25,2]).on('zoom', zoomed));

        metadata.forEach(function(meta){
          var hierarchyContext = Object.assign({},meta);
          hierarchyContexts.push(hierarchyContext);
          hierarchyContext.index = 'h'+meta.key;
          hierarchyContext.filter = meta.filter
          hierarchyContext.transformY = height*meta.transform;
          hierarchyContext.root = {parent: null,id: meta.key,name: meta.key}
          hierarchyContext.dom = transform

          var layers = makePopulationIntoHierarchies(hierarchyContext.key,hierarchyContext.filter.allFiltered(),mapdata)
          hierarchyContext.layers={}
          layers.forEach(function(l,i,a){
            var dom = hierarchyContext.dom.append('g').attr('id',l.id)
            var join = dom.selectAll('.dnode' )
            var ty = hierarchyContext.transformY + scaleY(i*hierarchyContext.scale)/a.length
             hierarchyContext.layers[l.id]={
              layerid: l.id,
              dom: dom,
              join: join,
              transformY: ty,
              hierarchy: l,
              tree:tree().size([scaleX(1), scaleY(1*hierarchyContext.scale)/a.length])
            }
          })
              //.attr("id",hierarchyContext.index)
              // .attr("transform", "translate(0 ," + hierarchyContext.transformY + ")");

          //hierarchyContext.hlinks = hierarchyContext.g1;
          //hierarchyContext.nodes = hierarchyContext.g1;
          //hierarchyContext.mlinks = hierarchyContext.g1;
        })
        update()
        //svg.append('g').attr("class", "brush").call(zoomBrush)
    }
    let update= ()=>{
      displayedNodeIndex={};
      hierarchyContexts.forEach(function(context){
        updateHierarchy(context,mapping)
          indexNodes(context)
        })
        updateInterlinks(mapping)
        updateDAG()
    }

    let updateHierarchy=(hc,mapdata)=>{
      var hierarchyContext = hc;

      Object.keys(hierarchyContext.layers).forEach(function(key){
        var l = hierarchyContext.layers[key];
        l.hierarchy.children=[];
      })

      var af = hierarchyContext.filter.allFiltered()
      var layers = makePopulationIntoHierarchies(hierarchyContext.key,hierarchyContext.filter.allFiltered(),mapdata)
      layers = layers.forEach(function(l,i,a){
        var layer = hierarchyContext.layers[l.id]
        layer.hierarchy = l
        layer.tree(layer.hierarchy);
        layer.hierarchy.each((n)=>n.y=n.y+layer.transformY)
      })
      hierarchyContext.allNodes = [];
      hierarchyContext.allLeaves = [];
      hierarchyContext.map = {};

      Object.keys(hierarchyContext.layers).forEach(function(key){
        var l = hierarchyContext.layers[key]
        var nodesToDisplay = l.hierarchy.descendants();
        // if(selectionFocus)
        //   nodesToDisplay = nodesToDisplay.filter(function(n){
        //     return n.data.visibilty == "on"});

        hierarchyContext.allNodes = hierarchyContext.allNodes.concat(l.hierarchy.descendants());
        hierarchyContext.allLeaves = hierarchyContext.allLeaves.concat(l.hierarchy.leaves());

        l.hierarchy.each(function(n){hierarchyContext.map[n.data.id] = n;});

        // l.hierarchy.each(function(n){
        //   n.y = n.y+hierarchyContext.transformY;
        //   if(hierarchyContext.fixed){
        //     n.fx=n.x
        //     n.fy=n.y
        //     n.fixed=true
        //   }
        // })

        var data = l.hierarchy.leaves();
        var d = [];

        var join = svg.selectAll('#'+l.layerid)
        var joinSelection = join.selectAll('.dnode').data(data,function(d){
          return d.id
        });

        joinSelection
        .transition().duration(TRANSITION_DURATION)
        .attr('transform', function(d) {
          var r ='';
          r = 'translate(' + (d.x ) + ','+(d.y)+')';
          return r })

        joinSelection.exit().remove();

        var n = joinSelection.enter().append('g').attr('opacity',OPACITY.NODE_DEFAULT).attr('class', 'dnode');
        n
        .attr('transform', function(d) {
            var r='';
            if(d.parent)
              r = 'translate(' + (d.parent.x ) + ','+(d.parent.y)+')'
            return r;})
         .transition()
         .duration(TRANSITION_DURATION)
          .attr('transform', function(d) {
            var r = 'translate(' + (d.x ) + ','+d.y+')';
            return r })

            n.append('circle')
              .attr('r',10)
              .attr('class', hierarchyContext.class)
              .attr('z-index',100)
              .on('mousedown', mouseSelected)
              .on('mouseup', mouseDeselected)
              .on('mouseover', mouseovered)
              .on('mouseout', mouseouted)
              .on('click', focus)
              .on('dblclick', toggleHierarchy)

            // n.append("text")
            //  .classed("node",true)
            //   .attr("dy", "0.31em")
            //   .attr("transform", function(d) {
            //     var r = "translate(0,-10) rotate(-45)";
            //     return r })
            //   .attr("z-index",100)
            //   .attr("text-anchor", function(d) { return d.x > -1 ? "start" : "end"; })
            //   .text(function(d) { return d.data.name; })
            //
              //.on("click", navigate

              n.append('text')
               .classed('vnode',true)
                //.classed("leaf",function(d){ return !d.children})
                .append('textPath').attr('href','#cpath')
                  .text(function(d) { return d.data.name; })
                  .on('click', navigate)
      })

          function toggleHierarchy(node) {
            node.data.childState = node.data.childState === 'collapsed' ? 'expanded' : 'collapsed';
            var orignode = hierarchyContext.map[node.id];
            orignode.each(function(n){
              n.data.state = node.data.childState === 'collapsed' ? 'collapsed' :  'expanded';
            });
            node.data.state = 'expanded';
            update();
          }

          function focus(node) {
            if(!selectionFocus){
              selectionFocus=true
              var res = computeRelated(node)
              Object.keys(res.nodes).forEach(function(id){
                var nd = displayedNodeIndex[id]
                if(nd)
                  nd.data.visibility='on'
              })



            } else{
              selectionFocus=false
              Object.keys(displayedNodeIndex).forEach(function(id){
                var nd = displayedNodeIndex[id]
                if(nd)
                  delete nd.data.visibility
              })

            }
            update();
          }

          function mouseouted(node) {
              hideTooltip();
              defaultView(svg.selectAll('.dnode'),svg.selectAll('.ilink'),node);
          }

          function mouseovered(node) {
            tooltip.html('<b>'+node.data.name+'</b><br/><i>'+node.data.comment+'</i><br/>'+node.data.zone);
            showTooltip();

              fadeView(svg.selectAll('.dnode'),svg.selectAll('.ilink'),node);
              var res = computeRelated(node)
              var nf = svg.selectAll('.dnode').filter(function(d){
                return res.nodes[d.id]
              })
              var lf = svg.selectAll('.ilink').filter(function(d){
                return res.links[d.source.id+d.target.id]
              })
            highlightView(nf,lf,node);
          }

          function mouseSelected(node) {

          }
          function mouseDeselected(node) {

          }

          function computeRelated(node){
            var id = node.data.id;
            var result = {nodes: {},links: {}}
            result.nodes[node.id]=true
            var linkids = {}
            function crn(id){
              var rel = mapping.filter(function(linkdata){
                var linkid = linkdata.source+linkdata.target;

                //console.log(linkdata)
                if(linkids[linkid]){
                  return false;
                } else if(linkdata.type==='Dependency'){
                  return linkdata.source===id
                } else if(linkdata.type==='Realisation')
                {
                  return linkdata.target===id
                }
              })
              if(rel.length>0){
                rel.forEach(function(link){
                  if(link.source===id){
                    result.nodes[link.target]=true;
                  }else {
                    result.nodes[link.source]=true;
                  }
                  var ld = ilinkindex[link.source+link.target]
                  result.links[link.source+link.target]=true
                  linkids[link.source+link.target]=true
                  crn(link.target)
                })
              }
              return rel;
            }
            crn(id);
            return result;
          }
    }
    let indexNodes= (hierarchyContext)=>{
      hierarchyContext.allNodes.forEach(function(node){
        displayedNodeIndex[node.id]=node
      })
    }
    let updateInterlinks =(mapping)=>{
        var ilink =  ilinks.selectAll('.ilink');
        ilinkindex={};
        ilinkdata = interRelationships(mapping)

        function interRelationships(mapping) {
          var map = {},map2={}, relationships = [];
          if(showDependencies){
            mapping.forEach(function(i) {
                var s =  displayedNodeIndex[i.source]
                var t =  displayedNodeIndex[i.target];
                if(s && t){
                  var t1 = {source: s, target: t}
                  ilinkindex[s.id+t.id]=t1
                  relationships.push(t1);
                }
              });
          }
          return relationships;
        }

        ilink = ilink.data(ilinkdata,function(d){
          return d.source.id+d.target.id;});

        ilink.transition()
             .duration(TRANSITION_DURATION)
             .attr('d',function(d){
               var points = [
               {x: d.source.x,y: d.source.y},
               {x: d.target.x,y: d.source.y},
               {x: d.target.x,y: d.target.y}]
               var p = cline(points)
               return p;
             })

        ilink.exit().transition().remove();
        ilink.enter().append('path')
            .each(function(d) {
            })
            .attr('opacity',OPACITY.LINK_DEFAULT)
            .attr('class', 'ilink')
            .attr('style','marker-end: url(#triangle)')
            .attr('stroke-dasharray','2,5')
            .attr('d',function(d){
              var points = [
                {x: d.source.x,y: d.source.y},
                {x: d.target.x,y: d.source.y},
                {x: d.target.x,y: d.target.y}]
              var p = cline(points)
              return p;
            })
    }
    let updateDAG= ()=>{
      var allNodes =[]
      hierarchyContexts.forEach(function(context){
        allNodes = allNodes.concat(context.allLeaves);
      })
    }

  // var allServices =[]
  //     hierarchyContexts.forEach(function(context){
  //       if(context.key==='service')
  //         allServices = context.allLeaves;
  //     })

  useEffect(()=>{
    render()
  })
  
  return <div id={name}></div>
}

