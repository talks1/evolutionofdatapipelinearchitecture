
export const Projects = (identifier)=>{
    
    const CreateProject = (identifier,theparent=null)=>{
        let project = {
            id: identifier,
            name: identifier,
            children : [],
            parent : theparent,
            axis: {}
        }
    
        project.Identifier= ()=>project.id,  
        project.Done= ()=>project.parent ? project.parent : project
        project.Children = ()=>project.children
        project.Profitability= (n)=>{
            if(n==undefined) return project.axis.profitability
            else project.axis.profitability=n
            return project
        },
        project.CustomerValue= (n)=>{
            if(n==undefined) return project.axis.customerValue
            else project.axis.customerValue=n
            return project
        },
        project.Timeliness= (n)=>{
            if(n==undefined) return project.axis.timeliness
            else project.axis.timeliness=n
            return project
        },
        project.Resource= (n)=>{
            if(n==undefined) return project.axis.resource
            else project.axis.resource=n
            return project

        },
        project.Languages= (n)=>{
            if(n==undefined) return project.axis.languages
            else project.axis.languages=n.split(',').length
            return project     
        },
        project.Storage= (n)=>{
            if(n==undefined) return project.axis.storage
            else project.axis.storage=n.split(',').length
            return project
        },
        project.Schema= (n)=>{
            if(n==undefined) return project.axis.schema
            else project.axis.schema=n
            return project
        },
        project.DataPoints= (n)=>{
            if(n==undefined) return project.axis.datapoints
            else project.axis.datapoints=n
            return project
        },
        project.QueueMechanism= (n)=>{
            if(n==undefined) return project.axis.queueMechanism
            else project.axis.queueMechanism=n.split(',').length
            return project
        },
        project.TeamLocations= (n)=>{
            if(n==undefined) return project.axis.teamLocationsd
            else project.axis.teamLocations=n.split(',').length
            return project
        },
        project.MicroService= (n)=>{
            if(n==undefined) return project.axis.microService
            else project.axis.microService=n.split(',').length
            return project
        },
        project.Agileness= (n)=>{
            if(n==undefined) return project.axis.agileness
            else project.axis.agileness=n
            return project
        },
        project.SubSystems= (n)=>{
            if(n==undefined) return project.axis.subsystems
            else project.axis.subsystems=n.split(',').length
            return project
        },
        
        project.Project = (identifier)=>{
            let child = CreateProject(identifier,project)
            project.children.push(child)
            return child
        }
        return project
    }

    const projects = CreateProject(identifier,null)
    return projects
}

export const Scale = () => {

    const CreateScale = ()=>{
        let scale = {
            unit: 0
        }
        scale.Unit= (n)=>{
            if(n==undefined) return scale.profitability
            else scale.profitability=n
            return scale
        }

        return scale
    }

    return CreateScale()
}