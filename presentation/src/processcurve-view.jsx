import React, { useReducer , useState, useEffect } from 'react'
import { HistogramComponent } from './histogram-component'

import Select from 'react-select';
// import { extent } from 'd3-array'
// import { scaleLinear } from 'd3-scale'

const diceOptions = [
{ label: 'Teams with few dependencies', value: 1.0 },
{ label: 'Teams who limit WIP (kanban)', value: 1.25 },
{ label: 'Team who do batch iterations (Scrum)', value: 1.5},
{ label: 'Teams who do large batches', value: 2}
]

function randomWeibull(alpha, lambda, location = 0.0) {
  // reference: https://en.wikipedia.org/wiki/Weibull_distribution  

  // some guarding against null and force positive values
  alpha = alpha == null ? 0 : +alpha;
  lambda = lambda == null ? 0 : +lambda;
  
  return function() {
    return location + (lambda * Math.pow(-Math.log(Math.random()), (1.0/alpha)));
  };
}

const makeSampleData = (option)=>{
  return Array.from({length: 100000}, randomWeibull(option.value, 5))
}

export const ProcessCurveView = ({name='processcurve'})=>{

  const [ diceSelections, setDiceSelections] = useState(diceOptions[0])
  
  let initialData = makeSampleData(diceSelections)
  const [sampleData,setSampleData] = useState(initialData)

  useEffect(()=>{
    let sData = makeSampleData(diceSelections)
    setSampleData(sData)
  },[diceSelections,setSampleData])

  function diceChange(option){
    setDiceSelections(option)
  }
  
  return <div>
     <Select
        value={diceSelections}
        onChange={diceChange}
        options={diceOptions}
      />
      <HistogramComponent name={name} sampleData={sampleData} width="400" height="400" />
      </div>
}
