import React, { useEffect, useState } from 'react';

import { format } from 'd3-format'
import {select} from 'd3-selection'
import { hierarchy, partition } from 'd3-hierarchy'
import {scaleOrdinal, scaleLinear,scaleQuantize} from 'd3-scale'
import {schemeCategory10} from 'd3-scale-chromatic'
import {transition} from 'd3-transition'
import { axisBottom, axisLeft } from 'd3-axis'
import { extent, max,min } from 'd3-array'
import regression from 'regression'

const coarseness = 50

export const interpolate = ({x1,x2,y1,y2}, model) =>{
	
	let dx = x2-x1 
	let dy = y2-y1 
	let min = Math.min(dx,dy)
	let result = [...Array((coarseness+1) * (coarseness+1))]

	let raw =  result.map((item,index)=>{
	let i = Math.floor(index/(coarseness+1))
	let j = index - (i*(coarseness+1))

	let ux = i/coarseness
	let uy = j/coarseness

	let x = model.xScaleUnit.invert(ux)
	let y = model.yScaleUnit.invert(uy)
	let z = model.z(ux,uy) 

	return {id: `${i}-${j}`,i,j,x,y,z,ux,uy}
	})

	let zBounds = extent(raw.map(item=>item.z))
	
	let data  =raw.map(cell=>{
		return {...cell,z:cell.z}
	})

	let res =  {
		xaxis: model.xaxis,
		yaxis: model.yaxis,
		data
	}

	//console.log(min)
	//console.log(res)
	return res
}

export const Model = (sampleData, regressionType) => {
	const xExtents = extent(sampleData, d => d[0])
	console.log(xExtents)
	const xScaleUnit = scaleLinear()
		.domain(xExtents)
		.range([0,1])
	
	const yExtents = extent(sampleData, d => d[1])
	const yScaleUnit = scaleLinear()
		.domain(yExtents)
		.range([0,1])
	const dy = yExtents[1]-yExtents[0]

	// console.log(xExtents)
	// console.log(yExtents)

	const unitData = sampleData.map(d=>{
		let x = d[0]
		let y = d[1]
		let ux = xScaleUnit(x)
		let uy = yScaleUnit(y) 
		return [ux,uy]
		})

	let regressionModel = null
	let coefficents = null
	
	if(regressionType==='none'){

	} else if(regressionType==='linear'){
		regressionModel = regression.linear(unitData)
		coefficents = regressionModel.equation
	}else {
		regressionModel = regression.polynomial(unitData, { order: 2 });
		coefficents = regressionModel.equation	
	}

	return {
		xScaleUnit,
		yScaleUnit,
		x: (i)=>{
			let result = xScaleUnit.invert(i)
			return result
		},
		y: (j)=>{
			let result = yScaleUnit.invert(j)
			return result
		},
		z: (i,j)=>{

			let my=null
			if(regressionType==='none'){
				let match = unitData.find(ud=> Math.floor(ud[0]*50)===Math.floor(i*50) && Math.floor(ud[1]*50)===Math.floor(j*50))
				return match ? 1 : 0
			} else if(regressionType==='linear'){
				my = coefficents[0]*i + coefficents[1]
			}else {
				my = coefficents[0]*i*i + coefficents[1]*i+ coefficents[2]
			}
			

			let diff = Math.abs(my-j)
			diff = diff > 1? 1 : (diff < 0 ? 0 : diff)
			return (1 - diff)*0.9+0.1
		},
		xaxis: 'XAxis',
		yaxis: 'YAxis'
	}
}

export const HeatmapComponent =  ({ name, sampleData, regressionType}) => {

	// console.log('SD')
	// console.log(regressionType)

	const margin = {top: 20, right: 20, bottom: 30, left: 40}

	const [theVis, setVis] = useState(null);
	let thevis = theVis

	const setup = () => {

		if(!thevis){
			thevis = visualization(`#${name}`, {
				//size: 300,
			});
			thevis.render(sampleData,regressionType);
			setVis(thevis)
		}
		thevis.update(sampleData,regressionType)
	}

	var visualization = function(container, configuration) {
		var that = {};
		var config = {
			width: 500,
			height: 500
		};

		function configure(configuration) {
			var prop = undefined;
			for ( prop in configuration ) {
				config[prop] = configuration[prop];
			}
			
		}
		that.configure = configure;

		const extents = {	x1: margin.left, y1: margin.top, x2: config.width - margin.right, y2: config.height - margin.bottom} 
		const cell = Math.min(extents.x2-extents.x1,extents.y2-extents.y1)/coarseness
	  
		function render(sData,regressionType) {
			that.svg = select(container)
				.append('svg:svg')
					.attr('class', 'visualization')
					.attr('width', config.width)
					.attr('height', config.height)
					.attr('viewBox', [0, 0, config.width, config.height])
					.style('font', '16px sans-serif');
						
			update(sData,regressionType);
		}
		that.render = render;
	
		function update(sData, regressionType) {
			// if ( newConfiguration  !== undefined) {
			// 	configure(newConfiguration);
			// }

			const xScale = scaleLinear()
			.domain(extent(sData, d => d[0]))
			.range([margin.left, config.width - margin.right])

			const yScale = scaleLinear()
				.domain(extent(sData, d => d[1]))
				.rangeRound([config.height - margin.bottom, margin.top])

			const xAxis = g => g
				.attr("transform", `translate(0,${config.height - margin.bottom})`)
				.call(axisBottom(xScale).ticks(config.width / 80, ""))
				.call(g => g.select(".domain").remove())
				.call(g => g.append("text")
					.attr("x", config.width - margin.right)
					.attr("y", -4)
					.attr("fill", "currentColor")
					.attr("font-weight", "bold")
					.attr("text-anchor", "end")
					.text(data => interpolatedData.xaxis)
				)

			const yAxis = g => g
				.attr("transform", `translate(${margin.left},0)`)
				.call(axisLeft(yScale).ticks(null, ""))
				.call(g => g.select(".domain").remove())
				.call(g => g.append("text")
					.attr("x", 4)
					.attr("y", margin.top)
					.attr("dy", ".71em")
					.attr("fill", "currentColor")
					.attr("font-weight", "bold")
					.attr("text-anchor", "start")
					.text(data=>interpolatedData.yaxis)
					)

			var colorScale = scaleQuantize()
					.domain([0,1])
					.range(["#FFFFFF", "#AAAAAA","#888888","#444444","#000000"]);
			
					
			const model = Model(sData,regressionType)
			const interpolatedData = interpolate(extents,model )

			// console.log(sampleData)
			// console.log(interpolatedData.data.map(d=>d.z))

				that.svg.selectAll('*').remove()

				let cells = that.svg.append('g')
					.selectAll("rect")
					.data(interpolatedData.data)

				cells.enter()
						.append('rect')
							.attr('id',d=>d.id)
							.attr("fill", d=>{
								return colorScale(d.z)
							})
							.attr("width",cell)
							.attr("height",cell)
							.attr("transform", d => {
								let coordX = xScale(d.x)
								return `translate(${xScale(d.x)},${yScale(d.y)})`
							})

				that.svg.append("g").call(xAxis);
				that.svg.append("g").call(yAxis);
		}
		that.update = update;

		configure(configuration);
			
		return that;
	};

  useEffect(setup)
  
  return <div id={name}></div>
}




